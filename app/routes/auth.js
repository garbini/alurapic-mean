/*! Auth route module ***/

module.exports = function (app) {

  var api = app.api.auth;

  app.post('/v1/auth', function (req, res) {
    api.auth(req, res, app);
  });

  app.use('/*', function (req, res, next) {
    api.checkToken(req, res, app, next);
  });

};



/*! End ***/