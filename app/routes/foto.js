/*! Foto route module ***/

module.exports = function (app) {

  var foto = app.api.foto;

  app.route('/v1/fotos')
    .get(foto.restore)
    .post(foto.create);

  app.route('/v1/fotos/:id')
    .get(foto.restoreById)
    .put(foto.update)
    .delete(foto.delete);

};



/*! End ***/