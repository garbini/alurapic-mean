/*! Auth model ***/

var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');
var model = mongoose.model('Usuario');



/*! Model actions ***/

var Auth = function () {};

Auth.prototype.auth = function (req, res, app) {

  model
    .findOne({ login: req.body.login, senha: req.body.senha })
    .then(function (usuario) {

      if (!usuario) {
        console.log('Login e senha inválidos');
        res.sendStatus(401);
      } else {

        var token = jwt.sign({ login: usuario.login }, app.get('secret'), {
          expiresIn: 86400
        });

        console.log('Token enviado no header da requisição.');
        res.set('x-access-token', token);
        res.end();

      }

    }, function (error) {
      console.log(error);
      res.sendStatus(401);
    });

};

Auth.prototype.checkToken = function (req, res, app, next) {

  var token = req.headers['x-access-token'];

  if (token) {
    jwt.verify(token, app.get('secret'), function (err, decoded) {
      if (err) {
        console.log('Token rejeitado');
        res.sendStatus(401);
      }
      req.usuario = decoded;
      next();
    });
  } else {
    console.log('Token não enviado.');
    res.sendStatus(401);
  }

};



/*! Auth api module ***/

module.exports = function () {
  return new Auth();
};



/*! End ***/