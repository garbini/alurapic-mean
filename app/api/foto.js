/*! Foto model ***/

var mongoose = require('mongoose');
var model = mongoose.model('Foto');



/*! Model actions ***/

var Foto = function () {};



Foto.prototype.create = function (req, res) {

  var foto = req.body;

  model
    .create(foto)
    .then(function (foto) {
      res.json(foto);
    }, function (error) {
      console.log(error);
      res.status(500).json(error);
    });

};



Foto.prototype.restore = function (req, res) {

  model.find({})
    .then(function (fotos) {
      res.json(fotos);
    }, function (error) {
      console.log(error);
      res.status(500).json(error);
    });

};



Foto.prototype.restoreById = function (req, res) {

  model
    .findById(req.params.id)
    .then(function (foto) {
      if (!foto) throw Error('Foto não encontrada.');
      res.json(foto);
    }, function (error) {
      console.log(error);
      res.status(404).json(error);
    });

};



Foto.prototype.update = function (req, res) {

  var foto = {
    id: req.params.id,
    body: req.body
  };

  model
    .findByIdAndUpdate(foto.id, foto.body)
    .then(function (foto) {
      res.json(foto);
    }, function (error) {
      console.log(error);
      res.status(500).json(error);
    });

};



Foto.prototype.delete = function (req, res) {

  model
    .remove({ _id: req.params.id })
    .then(function () {
      res.sendStatus(204);
    }, function (error) {
      res.status(500).json(error);
    });

};



/*! Foto model module ***/

module.exports = function () {
  return new Foto();
};



/*! End ***/