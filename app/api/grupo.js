/*! Foto model ***/

var mongoose = require('mongoose');
var model = mongoose.model('Grupo');



/*! Model actions ***/

var Grupo = function () {};

Grupo.prototype.restore = function (req, res) {

  model.find({})
    .then(function (grupos) {
      res.json(grupos);
    }, function (error) {
      console.log(error);
      res.status(500).json(error);
    });

};



/*! Grupo api module ***/

module.exports = function () {
  return new Grupo();
};



/*! End ***/