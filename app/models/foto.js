/*! Required modules ***/

var mongoose = require('mongoose');



/*! Foto model ***/

module.exports = function () {

  var schema = mongoose.Schema({
    titulo: {
      type: String,
      required: true
    },
    url: {
      type: String,
      required: true
    },
    descricao: {
      type: String,
      required: false
    },
    grupo: {
      type: Object,
      required: true
    }
  });

  mongoose.model('Foto', schema);

};


/*! End ***/