/*! Required modules ***/

var mongoose = require('mongoose');



/*! Usuario model ***/

module.exports = function () {

  var schema = mongoose.Schema({
    login: {
      type: String,
      required: true
    },
    senha: {
      type: String,
      required: true
    }
  });

  mongoose.model('Usuario', schema);

};


/*! End ***/