/*! Required modules ***/

var mongoose = require('mongoose');



/*! Grupo model ***/

module.exports = function () {

  var schema = mongoose.Schema({
    nome: {
      type: String,
      required: true
    }
  });

  mongoose.model('Grupo', schema);

};


/*! End ***/