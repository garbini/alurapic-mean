/*! Mean server ***/

var http = require('http');
var app = require('./config/express');
var database = require('./config/database')();

var porta = process.env.PORT || 3000;

http.createServer(app).listen(porta, function () {
    console.log('# Server listening on http://localhost:3000');
});



/*! End ***/