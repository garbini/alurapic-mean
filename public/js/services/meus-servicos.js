/*! Serviços ***/

angular.module(
  'meusServicos',
  [
    'ngResource'
  ]
)



/*! Recurso fotos ***/

.factory('recursoFoto', function ($resource) {

  return $resource('v1/fotos/:id', null, {
    update: {
      method: 'PUT'
    }
  });

})



/*! Recurso cadastro fotos ***/

.factory('cadastroFoto', function (recursoFoto, $q, $rootScope) {

  var servico = {};
  var evento = 'fotoCadastrada';

  servico.cadastrar = function (foto) {

    return $q(function (resolve, reject) {

      if (foto._id) {

        recursoFoto.update({
          id: foto._id
        }, foto, function () {
          $rootScope.$broadcast(evento);
          resolve({
            mensagem: 'Foto ' + foto.titulo + ' atualizada com sucesso!',
            inclusao: false
          });
        }, function (erro) {
          console.log(erro);
          reject({
            mensagem: 'Erro: a foto não foi atualizada, tente novamente!'
          });
        });

      } else {

        recursoFoto.save(foto, function () {
          $rootScope.$broadcast(evento);
          resolve({
            mensagem: 'Foto ' + foto.titulo + ' cadastrada com sucesso!',
            inclusao: true
          });

        }, function (erro) {
          console.log(erro);
          reject({
            mensagem: 'Erro: foto não cadastrada, tente novamente!'
          });
        });

      }

    });

  };

  return servico;

});



/*! End ***/