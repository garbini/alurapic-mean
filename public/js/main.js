/*! Angular module ***/

angular.module(
  'alurapic',
  [
    'minhasDiretivas',
    'ngAnimate',
    'ngRoute',
    'meusServicos'
  ]
)
.config(function ($routeProvider, $httpProvider) {

  $httpProvider.interceptors.push('tokenInterceptor');

  $routeProvider.when('/fotos', {
    templateUrl: '/partials/principal.html',
    controller: 'FotosController'
  });

  $routeProvider.when('/fotos/new', {
    templateUrl: '/partials/foto.html',
    controller: 'FotoController'
  });

  $routeProvider.when('/fotos/edit/:id', {
    templateUrl: '/partials/foto.html',
    controller: 'FotoController'
  });

  $routeProvider.when('/login', {
    templateUrl: '/partials/login.html',
    controller: 'LoginController'
  });

  $routeProvider.otherwise({
    redirectTo: '/fotos'
  });

});