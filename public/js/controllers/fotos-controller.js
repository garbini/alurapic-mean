/*! Fotos controller ***/

angular.module('alurapic')
  .controller('FotosController', function ($scope, recursoFoto) {


  /*! Propriedades ***/

  $scope.fotos = [];

  $scope.filtro = '';

  $scope.mensagem = '';



  /*! Lista de fotos ***/

  recursoFoto.query(function (fotos) {
    $scope.fotos = fotos;
  }, function (erro) {
    console.log(erro);
  });



  /*! Remove foto ***/

  $scope.remover = function (foto) {

    recursoFoto.delete({
      id: foto._id
    }, function () {
      var indiceFoto = $scope.fotos.indexOf(foto);
      $scope.fotos.splice(indiceFoto, 1);
      $scope.mensagem = 'Foto ' + foto.titulo + ' foi removida com sucesso!';
    }, function (erro) {
      $scope.mensagem = 'Foto ' + foto.titulo + ' não foi removida!';
    });

  };



});



/*! End ***/