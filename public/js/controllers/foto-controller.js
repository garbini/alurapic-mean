angular.module('alurapic')
  .controller('FotoController', function ($scope, $routeParams, recursoFoto, cadastroFoto) {



  /*! Propriedades ***/

  $scope.foto = {};
  $scope.mensagem = '';



  /*! Editar ***/

  if ($routeParams.id) {

    recursoFoto.get({
      id: $routeParams.id
    }, function (foto) {
      $scope.foto = foto;
    }, function (erro) {
      $scope.mensagem = 'Não foi possível obter lista de fotos.';
    });

  }



  /*! Add ***/

  $scope.salvar = function () {

    if ($scope.formulario.$valid) {

      cadastroFoto
        .cadastrar($scope.foto)
        .then(function (retorno) {
          $scope.mensagem = retorno.mensagem;
          if (retorno.inclusao) $scope.foto = {};
        })
        .catch(function (retorno) {
          $scope.mensagem = retorno.mensagem;
          $scope.focado = false;
        });

    }

  };

});