angular.module('alurapic')
.controller('LoginController', function ($scope, $http, $location) {

  $scope.usuario = {};
  $scope.mensagem = '';

  $scope.autenticar = function () {

    var usuario = $scope.usuario;

    $http.post('/v1/auth', {
      login: usuario.login,
      senha: usuario.senha
    }).then(function () {
      $location.path('/');
    }, function (error) {

      $scope.usuario = {};
      console.log(error);
      $scope.mensagem = 'Login ou senha inválidos!';

    });

  };

});