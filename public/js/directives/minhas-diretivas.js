/*! Diretivas ***/

angular.module('minhasDiretivas', [])



/*! Painel de fotos ***/

.directive('meuPainel', function () {

    var ddo = {};

    ddo.restrict = 'AE';

    ddo.scope = {
        titulo: '@'
    };

    ddo.transclude = true;

    ddo.templateUrl = 'partials/components/meu-painel.html';

    return ddo;

})



/*! Foto ***/

.directive('minhaFoto', function () {

    var ddo = {};

    ddo.restrict = 'AE';

    ddo.scope = {
        titulo: '@',
        url: '@'
    };

    ddo.templateUrl = 'partials/components/minha-foto.html';

    return ddo;

})



/*! Botão periculozo ***/

.directive('botaoPerigo', function () {

    var ddo = {};

    ddo.restrict = 'AE';

    ddo.scope = {
        nome: '@',
        acao: '&'
    };

    ddo.templateUrl = 'partials/components/botao-perigo.html';

    return ddo;

})



/*! Focus ***/
.directive('meuFocus', function () {

    var ddo = {};

    ddo.restrict = 'A';

    ddo.link = function (scope, element) {

        scope.$on('fotoCadastrada', function () {
            element[0].focus();
        });

    };

    return ddo;

});



/*! End ***/