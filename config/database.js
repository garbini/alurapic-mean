/*! Required modules ***/

var mongoose = require('mongoose');



/*! Database connection & logs ***/

module.exports = function () {

  if (process.env.NODE_ENV == 'production') {

    var stringConexao = process.env.MONGOLAB_URI;
        stringConexao = stringConexao.match(/mongodb:\/\/(.*):(.*)@(.*)/);

    var uri  = 'mongodb://';
        uri += stringConexao[1];
        uri  = ':';
        uri += stringConexao[2];
        uri  = '@';
        uri += stringConexao[3];

    mongoose.connect(uri);

  } else {
    mongoose.connect('mongodb://localhost/alurapic');
  }

  mongoose.connection.on('connected', function () {
    console.log('# MongoDB ready');
  });

  mongoose.connection.on('error', function (error) {
    console.log('# MongoDB error: ' + error);
  });

  mongoose.connection.on('disconnected', function () {
    console.log('# MongoDB disconnected');
  });

  process.on('SIGINT', function () {
    mongoose.connection.close(function () {
      console.log('Bye.');
      process.exit(0);
    });
  });

};



/*! End ***/