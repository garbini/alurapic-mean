/*! Express config ***/

var express = require('express');
var load = require('express-load');
var bodyParser = require('body-parser');
var app = express();



/*! Express middlewares ***/

app.use(express.static('./public'));
app.use(bodyParser.json());



/*! Variaveis de ambiente ***/
app.set('secret', 'Foda-se filho da puta vai pra merda mano!?!?!');


/*! Load modules ***/

load('models', { cwd: 'app' })
  .then('api')
  .then('routes/auth.js')
  .then('routes')
  .into(app);



/*! Express config module ***/

module.exports = app;



/*! End ***/